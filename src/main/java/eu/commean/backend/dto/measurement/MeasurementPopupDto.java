package eu.commean.backend.dto.measurement;

import eu.commean.backend.entity.AverageTraffic;
import eu.commean.backend.entity.Node;
import eu.commean.backend.entity.TrafficMeasurement;
import eu.commean.backend.enums.TrafficSituation;
import eu.commean.backend.enums.VehicleTypes;
import eu.commean.backend.service.AverageTrafficService;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.log4j.Log4j2;

import java.util.UUID;

@Data
@AllArgsConstructor
@Log4j2

public class MeasurementPopupDto {

	private UUID nodeId;
	private String nodeName;
	private TrafficSituation trafficSituation;

	//TODO: Calculate wait time
	private int waitTime;

	public static MeasurementPopupDto convertToDto(TrafficMeasurement tm, Node node, AverageTrafficService avs) {
		TrafficSituation trafficSituation = TrafficSituation.NONE;
		//int currentTraffic = ((tm.getCars() + tm.getTrucks() + tm.getBus() + tm.getMotorbike()));

		//Change to Query where sum is made on database maybe?
		AverageTraffic averageTrafficMeasurement = avs.getAverageOfQuarterHour(VehicleTypes.ALL, tm.getNode().getId(),tm.getLine().getLineId(), tm.getTimestamp());
		log.debug("{}|{} Difference={}", averageTrafficMeasurement.getAvgCars(), tm.getCars(), averageTrafficMeasurement.getAvgCars() - tm.getCars());
		return new MeasurementPopupDto(node.getId(), node.getName(), trafficSituation, 0);


	}
}
