package eu.commean.backend.dto.measurement;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AverageVsNowDto {
    private String nodeName;
    private String lineName;
    private int cars;
    private int trucks;
    private int bus;
    private int motorbike;
    private int avgCars;
    private int avgTrucks;
    private int avgBus;
    private int avgMotorbike;
}
