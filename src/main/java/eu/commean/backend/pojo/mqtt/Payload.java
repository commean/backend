
package eu.commean.backend.pojo.mqtt;

import com.fasterxml.jackson.annotation.*;
import lombok.NoArgsConstructor;

import javax.annotation.Generated;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "count",
        "id",
        "time",
        "lines"
})
@Generated("jsonschema2pojo")
public class Payload {

    @JsonProperty("count")
    private Count count;
    @JsonProperty("id")
    private String id;
    @JsonProperty("time")
    private double time;
    @JsonProperty("lines")
    private List<Lines> lines = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public Payload(Count count, String id, double time, List<Lines> lines) {
        this.count = count;
        this.id = id;
        this.time = time;
        this.lines = lines;
    }

    @JsonProperty("count")
    public Count getCount() {
        return count;
    }

    @JsonProperty("count")
    public void setCount(Count count) {
        this.count = count;
    }

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("time")
    public double getTime() {
        return time;
    }

    @JsonProperty("time")
    public void setTime(int time) {
        this.time = time;
    }

    @JsonProperty("lines")
    public List<Lines> getLines() {
        return lines;
    }

    @JsonProperty("lines")
    public void setLines(List<Lines> lines) {
        this.lines = lines;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(Payload.class.getName()).append('@').append(Integer.toHexString(System.identityHashCode(this))).append('[');
        sb.append("count");
        sb.append('=');
        sb.append(((this.count == null) ? "<null>" : this.count));
        sb.append(',');
        sb.append("id");
        sb.append('=');
        sb.append(((this.id == null) ? "<null>" : this.id));
        sb.append(',');
        sb.append("time");
        sb.append('=');
        sb.append(this.time);
        sb.append(',');
        sb.append("lines");
        sb.append('=');
        sb.append(((this.lines == null) ? "<null>" : this.lines));
        sb.append(',');
        sb.append("additionalProperties");
        sb.append('=');
        sb.append(((this.additionalProperties == null) ? "<null>" : this.additionalProperties));
        sb.append(',');
        if (sb.charAt((sb.length() - 1)) == ',') {
            sb.setCharAt((sb.length() - 1), ']');
        } else {
            sb.append(']');
        }
        return sb.toString();
    }

}