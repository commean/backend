package eu.commean.backend.service;

import eu.commean.backend.entity.AverageTraffic;
import eu.commean.backend.enums.VehicleTypes;

import java.sql.Timestamp;
import java.util.UUID;

public interface AverageTrafficService {

    AverageTraffic getAverageOfQuarterHour(VehicleTypes vT, UUID uuid,UUID lineId, Timestamp timestamp);
}


