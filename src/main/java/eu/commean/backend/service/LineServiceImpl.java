package eu.commean.backend.service;

import eu.commean.backend.entity.Line;
import eu.commean.backend.entity.Node;
import eu.commean.backend.repo.LineRepository;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class LineServiceImpl implements LineService {

    final LineRepository lineRepository;

    public LineServiceImpl(LineRepository lineRepository) {
        this.lineRepository = lineRepository;
    }

    @Override
    public Line addLine(Line line) {
        return lineRepository.save(line);
    }

    @Override
    public Line updateLine(Line line) {
        return lineRepository.save(line);
    }

    @Override
    public Line removeLine(Line line) {
        return lineRepository.save(line);
    }

    @Override
    public Line addNode(Node node) {
        return null;
    }

    @Override
    public Line getLine(UUID id) {
        return lineRepository.findByLineId(id).orElse(new Line(new UUID(0,0),"null",null));
    }

    @Override
    public boolean exists(UUID id) {
        return lineRepository.existsByLineId(id);
    }
}
