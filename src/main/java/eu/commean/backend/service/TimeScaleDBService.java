package eu.commean.backend.service;

import eu.commean.backend.entity.TimeScaleDB;

public interface TimeScaleDBService {

    boolean checkForHypertable();

    TimeScaleDB createHypertable();

    boolean checkForContinuousAggregate();

    void createContinuousAggregate();

    void setRealTimeUpdate(boolean enable);

    int setRefreshPolicy();
}
