package eu.commean.backend.service;

import eu.commean.backend.entity.TimeScaleDB;
import eu.commean.backend.repo.TimeScaleDBRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TimeScaleDBServiceImpl implements TimeScaleDBService {

    TimeScaleDBRepository timeScaleDBRepository;

    @Autowired
    public TimeScaleDBServiceImpl(TimeScaleDBRepository timeScaleDBRepository) {
        this.timeScaleDBRepository = timeScaleDBRepository;
    }

    @Override
    public boolean checkForHypertable() {
        return timeScaleDBRepository.checkForHypertable();
    }

    @Override
    public TimeScaleDB createHypertable() {
        return timeScaleDBRepository.createHypertable();
    }

    @Override
    public boolean checkForContinuousAggregate() {
        return timeScaleDBRepository.checkForContinuousAggregate();
    }

    @Override
    public void createContinuousAggregate() {
        timeScaleDBRepository.createContinuousAggregate();
    }

    @Override
    public void setRealTimeUpdate(boolean enable) {
        timeScaleDBRepository.setRealTimeUpdate(!enable);
    }

    @Override
    public int setRefreshPolicy() {
        return timeScaleDBRepository.setRefreshPolicy();
    }


}
