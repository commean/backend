package eu.commean.backend.service;

import eu.commean.backend.entity.AverageTraffic;
import eu.commean.backend.enums.VehicleTypes;
import eu.commean.backend.repo.AverageTrafficRepository;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.TimeZone;
import java.util.UUID;
@Log4j2
@Service
public class AverageTrafficServiceImpl implements AverageTrafficService {
    AverageTrafficRepository averageTrafficRepository;

    int[] dowConversion = {0,6,0,1,2,3,4,5}; //Converts java dow to postgres dow (java start with 1=Sunday postgres starts with 0=Monday)

    @Autowired
    public AverageTrafficServiceImpl(AverageTrafficRepository averageTrafficRepository) {
        this.averageTrafficRepository = averageTrafficRepository;
    }

    @Override
    public AverageTraffic getAverageOfQuarterHour(VehicleTypes vT, UUID uuid,UUID lineId, Timestamp timestamp) {

        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH:mm");
        //ZonedDateTime time = ZonedDateTime.ofInstant(timestamp.toInstant(),ZoneId.of("UTC"));
        ZonedDateTime time = ZonedDateTime.now(ZoneId.of("UTC"));
        time = time.plusMinutes(15-time.getMinute()%15);

        log.debug("Timezone:{}, Time:{}, Formatted:{}",time.getZone(),time.toString(),time.format(dtf));
        log.debug("DOW:{}:{}",time.getDayOfWeek().getValue(),time.getDayOfWeek());
        if (vT != VehicleTypes.ALL)
            return averageTrafficRepository.getAverageMeasurmentOfQarterHour(vT, uuid,lineId, dowConversion[time.getDayOfWeek().getValue()],time.format(dtf))
                    .orElse(new AverageTraffic(new UUID(0, 0),new UUID(0, 0), new Timestamp(0), 0, 0, 0, 0));
        else
            return averageTrafficRepository.getAverageMeasurmentOfQarterHourAllTypes(uuid,lineId, dowConversion[time.getDayOfWeek().getValue()],time.format(dtf))
                    .orElse(new AverageTraffic(new UUID(0, 0),new UUID(0, 0), new Timestamp(0), 0, 0, 0, 0));

    }
}
