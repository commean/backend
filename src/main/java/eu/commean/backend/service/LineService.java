package eu.commean.backend.service;

import eu.commean.backend.entity.Line;
import eu.commean.backend.entity.Node;

import java.util.UUID;

public interface LineService {
    Line addLine(Line line);

    Line updateLine(Line line);

    Line removeLine(Line line);

    Line addNode(Node node);

    Line getLine(UUID id);

    boolean exists(UUID id);


}
