package eu.commean.backend.enums;

public enum VehicleTypes {
	ALL("all"), CARS("avg_cars"), TRUCKS("avg_trucks"), MOTORBIKES("avg_motorbikes"), BUS("avg_bus");
	private String value;

	VehicleTypes(String value) {
		this.value = value;
	}

}
