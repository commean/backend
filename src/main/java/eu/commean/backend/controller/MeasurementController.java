package eu.commean.backend.controller;

import eu.commean.backend.dto.measurement.AverageVsNowDto;
import eu.commean.backend.dto.measurement.MeasurementPopupDto;
import eu.commean.backend.dto.measurement.TrafficMeasurementStatisticsRealtimeDto;
import eu.commean.backend.entity.AverageTraffic;
import eu.commean.backend.entity.Line;
import eu.commean.backend.entity.Node;
import eu.commean.backend.entity.TrafficMeasurement;
import eu.commean.backend.enums.VehicleTypes;
import eu.commean.backend.pojo.mqtt.Payload;
import eu.commean.backend.security.jwt.JwtProvider;
import eu.commean.backend.service.AverageTrafficService;
import eu.commean.backend.service.LineService;
import eu.commean.backend.service.NodeService;
import eu.commean.backend.service.TrafficMeasurementService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Log4j2
@RestController
@RequestMapping("api/v1/measurements")
public class MeasurementController {


    TrafficMeasurementService tms;

    AverageTrafficService avs;

    LineService lineService;
    NodeService nodeService;
    JwtProvider jwtProvider;

    @Autowired
    public MeasurementController(TrafficMeasurementService tms, AverageTrafficService avs, LineService lineService, NodeService nodeService, JwtProvider jwtProvider) {
        this.tms = tms;
        this.avs = avs;
        this.lineService = lineService;
        this.nodeService = nodeService;
        this.jwtProvider = jwtProvider;
    }


    @GetMapping(value = "/{node}/{line}/info", produces = "application/json")
    @ResponseStatus(code = HttpStatus.OK)

    public Object getInfoFromNode(@PathVariable("node") UUID nodeId, @PathVariable("line") UUID lineUUID) {

        TrafficMeasurement tm = tms.getLatestMeasurementFromId(nodeId, lineUUID);
        if (tm == null) {
            log.error("No Measurements where found in the last minute for TCN from Node: {}, Line: {}", nodeId, lineUUID);
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No Node with given id");

        } else {
            return MeasurementPopupDto.convertToDto(tm, nodeService.getNodeById(tm.getNode().getId()), avs);

        }
    }

    @GetMapping(value = "/{node}/{line}/now", produces = "application/json")
    @ResponseStatus(code = HttpStatus.OK)

    public Object getNewestMeasurementFromNode(@PathVariable("node") UUID nodeId, @PathVariable("line") UUID lineUUID) {

        TrafficMeasurement tm = tms.getLatestMeasurementFromId(nodeId, lineUUID);
        if (tm == null) {
            log.error("No Measurements where found in the last minute for TCN from Node: {}, Line: {}", nodeId, lineUUID);
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No Node with given id");

        } else {
            return TrafficMeasurementStatisticsRealtimeDto.convertToDto(tm, nodeService.getNodeById(tm.getNode().getId()));

        }
    }

    @GetMapping(value = "/{node}/{line}/compare", produces = "application/json")
    public Object getComparisonAverageVsNowFromNode(@PathVariable("node") String idToken, @PathVariable("line") String lineIdToken) {
        UUID nodeUUID;
        UUID lineUUID;
        nodeUUID = convertStringToUUID(idToken);
        lineUUID = convertStringToUUID(lineIdToken);


        TrafficMeasurement tm = tms.getLatestMeasurementFromId(nodeUUID, lineUUID);
        if (tm == null) {
            log.error("No Measurements where found in the last minute for TCN with id: {}", nodeUUID);
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No Node with given id");

        } else {
            AverageTraffic averageTraffic = avs.getAverageOfQuarterHour(VehicleTypes.ALL, nodeUUID, lineUUID, tm.getTimestamp());
            if (averageTraffic == null) {
                log.error("No average found for node {} at {}", nodeUUID, tm.getTimestamp());
                throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No Average with given id or time");
            }
            return new AverageVsNowDto(
                    tm.getNode().getName(),
                    tm.getLine().getName(),
                    tm.getCars(),
                    tm.getTrucks(),
                    tm.getBus(),
                    tm.getMotorbike(),
                    averageTraffic.getAvgCars(),
                    averageTraffic.getAvgTrucks(),
                    averageTraffic.getAvgBus(),
                    averageTraffic.getAvgMotorbikes());
        }
    }

    @PostMapping(path = "", consumes = "application/json")
    @ResponseStatus(code = HttpStatus.CREATED)
    public void createNewMeasurement(@RequestHeader(name = "Authorization") String token, @RequestBody Payload payload) {
        UUID uuid;
        token = token.substring(7);
        log.debug("Token {}", token);
        String token_id = jwtProvider.getUsernameFromJWT(token);
        log.debug("Id {}", token_id);

        uuid = convertStringToUUID(token_id);

        Node tcn = nodeService.getNodeById(uuid);

        log.debug("UUID: {}", uuid);
        if (!uuid.equals(new UUID(0, 0))) {
            log.debug("Measurement: {}", payload);
            if (payload.getTime() == 0)
                log.error("Invalid timestamp");

            //Create measurement for each line on a node
            if (payload.getLines() != null) {
                List<TrafficMeasurement> trafficMeasurements = new ArrayList<>();
                payload.getLines().forEach(line -> {
                    UUID line_uuid = convertStringToUUID(line.getId());
                    Line lineEntity;
                    if (!lineService.exists(line_uuid)) {
                        log.debug("Create new line");
                        lineEntity = new Line(line_uuid, line.getName(), tcn);
                        lineService.addLine(lineEntity);
                    } else {
                        log.debug("Found existing line");
                        lineEntity = lineService.getLine(line_uuid);
                    }
                    log.debug(lineEntity.toString());
                    TrafficMeasurement tm = new TrafficMeasurement(
                            line.getCount().getTruck(),
                            line.getCount().getCar(),
                            line.getCount().getBus(),
                            line.getCount().getMotorbike(),
                            Timestamp.from(Instant.ofEpochSecond((int) payload.getTime())));
                    tm.setLine(lineEntity);
                    tm.setNode(nodeService.getNodeById(uuid));
                    trafficMeasurements.add(tm);


                });
                log.debug("Measurement List: {}", trafficMeasurements.size());
                tms.addTrafficMeasurement(trafficMeasurements);
            }

            //Create Measurement for all detected vehicles on node
            TrafficMeasurement tm = new TrafficMeasurement(
                    payload.getCount().getTruck(),
                    payload.getCount().getCar(),
                    payload.getCount().getBus(),
                    payload.getCount().getMotorbike(),
                    Timestamp.from(Instant.ofEpochSecond((int) payload.getTime())));

            tm.setNode(tcn);

            log.debug("Measurement: {}, {}, {}|TrafficCameraNode: {}", tm.getId(), tm.getNode().getName(),
                    tm.getTimestamp(), tcn.getId());

            tms.addTrafficMeasurement(tm);
        } else {
            log.warn("{} has no access to create Measurements", token_id);
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, "The given JWT cannot create measurements");
        }
    }


    // Exceptions
    @ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "Error in Json (Body)")
    @ExceptionHandler(HttpMessageNotReadableException.class)
    public void handleJsonParseException() {
        //Nothing to do because error is handled by Spring
    }

    public UUID convertStringToUUID(String token_id) {
        if (token_id.matches("^[a-fA-F\\d]{8}(?:\\-[a-fA-F\\d]{4}){3}\\-[a-fA-F\\d]{12}$"))
            return UUID.fromString(token_id);
        else
            return new UUID(0, 0);

    }

}
