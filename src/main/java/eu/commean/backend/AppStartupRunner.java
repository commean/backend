package eu.commean.backend;

import eu.commean.backend.config.MqttProperties;
import eu.commean.backend.entity.Node;
import eu.commean.backend.entity.TimeScaleDB;
import eu.commean.backend.mqtt.Client;
import eu.commean.backend.mqtt.UplinkCallback;
import eu.commean.backend.service.NodeService;
import eu.commean.backend.service.TimeScaleDBService;
import eu.commean.backend.service.TrafficMeasurementService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Log4j2
public class AppStartupRunner {

    TrafficMeasurementService measurementService;
    NodeService nodeService;
    TimeScaleDBService timeScaleDBService;
    private MqttProperties mqttProperties;

    @Autowired
    public AppStartupRunner(MqttProperties mqttProperties, TrafficMeasurementService measurementService, NodeService nodeService, TimeScaleDBService timeScaleDBService) {
        this.mqttProperties = mqttProperties;
        this.measurementService = measurementService;
        this.nodeService = nodeService;
        this.timeScaleDBService = timeScaleDBService;
    }


    @EventListener
    public void onApplicationEvent(ApplicationReadyEvent event) {
        //Subscribe to all Lora enabled Nodes (MQTT)
        Client client = new Client(mqttProperties);
        Client.create();
        this.subscribeToAllNodes(client);
        client.setCallback(new UplinkCallback(measurementService, nodeService));

        //Create Hypertable
        //TODO: execute in other way (Separate Repository, etc.)
        TimeScaleDB timeScaleDB = null;
        log.debug("Check for Hypertable");
        if (!timeScaleDBService.checkForHypertable()) {
            log.debug("Create Hypertable");
            timeScaleDB = timeScaleDBService.createHypertable();

        }
        if (timeScaleDB != null) {
            log.debug("Check if Hypertable was created");
            if (!timeScaleDB.getCreated()) {
                log.fatal("CREATION OF HYPERTABLE FAILED");
                System.exit(1);
            }
        }
        log.debug("Check for Continuous Aggregate");
        if (!timeScaleDBService.checkForContinuousAggregate()) {
            log.debug("Create Continuous Aggregate");
            timeScaleDBService.createContinuousAggregate();
            timeScaleDBService.setRefreshPolicy();
            //Deactivated since it slows down / usless since next data to compare to comes in 1 year, so it has plenty of time to refresh
            timeScaleDBService.setRealTimeUpdate(false);

            log.debug("---FINISHED FIRST TIME CONFIG---");
        }



    }

    private void subscribeToAllNodes(Client client) {

        List<Node> nodes = nodeService.getAllNodesWithTTNConnection();
        if (nodes != null) {
            for (Node node : nodes) {
                log.debug("{},{}", node.getId(), node.getTtnId());
            }
            nodes.forEach(node -> client.subscribe(node.getTtnId()));
            log.debug("Subscribed to {} node(s)", nodes.size());
        }
    }
}