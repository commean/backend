package eu.commean.backend.entity;

import lombok.*;
import org.hibernate.Hibernate;
import org.hibernate.annotations.Immutable;
import org.hibernate.annotations.Subselect;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;
import java.util.UUID;

@NamedNativeQuery(name = "AverageTraffic.getAverageMeasurmentOfQarterHour", query = "SELECT avg(:vehicle_type) AS :vehicle_type FROM traffic_measurement_per_day_avg WHERE dow = :dow AND bucket = :time AND node_id = :node_id AND line_id= :line_id", resultClass = AverageTraffic.class)
@NamedNativeQuery(name = "AverageTraffic.getAverageMeasurmentOfQarterHourAllTypes", query = "SELECT * FROM traffic_measurement_per_day_avg tmpda  WHERE dow = :dow AND node_id = :node_id AND line_id = :line_id AND bucket \\:\\: time = :time \\:\\: time", resultClass = AverageTraffic.class)

@Entity
@Immutable
@Getter
@Setter
@ToString
@RequiredArgsConstructor
@AllArgsConstructor
@Subselect("SELECT * FROM traffic_measurement_per_day_avg")
//stop creation of table https://stackoverflow.com/questions/33680504/exclude-a-specific-table-from-being-created-by-hibernate
@Table(name = "traffic_measurement_per_day_avg")
public class AverageTraffic {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "node_id", nullable = false)
    private UUID id;

    @Column(name = "line_id", nullable = false)
    private UUID line_id;

    private Timestamp bucket;

    @Column(name = "cars")
    private int avgCars;

    @Column(name = ("trucks"))
    private int avgTrucks;

    @Column(name = "motorbikes")
    private int avgMotorbikes;

    @Column(name = "bus")
    private int avgBus;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        AverageTraffic that = (AverageTraffic) o;
        return id != null && Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}
