package eu.commean.backend.entity;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.UUID;

@Entity
@Table(name = "line", schema = "public")
public class Line {
    @Id
    @Column(name = "id", nullable = false)
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "uuid2")
    private UUID id;

    @Column(name = "line_id")
    private UUID lineId;

    @Size(max = 255)
    @Column(name = "name")
    private String name;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "node_id")
    private Node node;

    @OneToMany(mappedBy = "line")
    private Set<TrafficMeasurement> trafficMeasurements = new LinkedHashSet<>();

    public Line(UUID lineId, String name, Node node) {
        this.lineId = lineId;
        this.name = name;
        this.node = node;
    }

    public Line() {

    }


    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getLineId() {
        return lineId;
    }

    public void setLineId(UUID lineId) {
        this.lineId = lineId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Node getNode() {
        return node;
    }

    public void setNode(Node node) {
        this.node = node;
    }

    public Set<TrafficMeasurement> getTrafficMeasurements() {
        return trafficMeasurements;
    }

    @Override
    public String toString() {
        return "Line{" +
                "id=" + id +
                ", lineId=" + lineId +
                ", name='" + name + '\'' +
                ", node=[" + node.getId()+
                "," + node.getName()+
                "]}";
    }

    public void setTrafficMeasurements(Set<TrafficMeasurement> trafficMeasurements) {
        this.trafficMeasurements = trafficMeasurements;
    }

}