package eu.commean.backend.entity;

import org.hibernate.annotations.Immutable;
import org.hibernate.annotations.Subselect;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedNativeQuery;


@NamedNativeQuery(name = "TimeScaleDB.createHypertable", query = "SELECT * FROM create_hypertable('traffic_measurement','timestamp',if_not_exists => true)", resultClass = TimeScaleDB.class)
@NamedNativeQuery(name = "TimeScaleDB.createContinuousAggregate", query = "CREATE MATERIALIZED VIEW traffic_measurement_time_grid WITH (timescaledb.continuous) AS SELECT time_bucket('15 minutes', \"timestamp\") AS bucket, node_id, line_id, CAST(floor(avg(cars)) AS int) AS avg_cars, CAST(floor(avg(trucks)) AS int) AS avg_trucks, CAST(floor(avg(motorbike)) AS int) AS avg_motorbikes, CAST(floor(avg(bus))AS int) AS avg_bus, CAST(floor(avg(EXTRACT(DOW FROM \"timestamp\")))AS int) AS dow FROM public.traffic_measurement GROUP BY bucket, node_id, line_id WITH NO DATA")
@NamedNativeQuery(name = "TimeScaleDB.setRefreshPolicy", query = "SELECT add_continuous_aggregate_policy('traffic_measurement_time_grid', start_offset => NULL, end_offset => INTERVAL '1 h', schedule_interval => INTERVAL '1 h') ;")
@NamedNativeQuery(name = "TimeScaleDB.setRealTimeUpdate", query = "ALTER MATERIALIZED VIEW traffic_measurement_time_grid set (timescaledb.materialized_only = true);")
@NamedNativeQuery(name = "TimeScaleDB.checkForHypertable", query = "SELECT CASE WHEN COUNT(ht) = 1 THEN TRUE ELSE FALSE END FROM timescaledb_information.hypertables ht WHERE ht.hypertable_name = 'traffic_measurement'")
@NamedNativeQuery(name = "TimeScaleDB.checkForContinuousAggregate", query = "SELECT CASE WHEN COUNT(ca) = 1 THEN TRUE ELSE FALSE END FROM timescaledb_information.continuous_aggregates ca WHERE ca.hypertable_name = 'traffic_measurement'")


@Entity()
@Subselect("SELECT * FROM traffic_measurement")
//stop creation of table https://stackoverflow.com/questions/33680504/exclude-a-specific-table-from-being-created-by-hibernate
@Immutable
public class TimeScaleDB {
    @Id
    @Column(name = "hypertable_id", nullable = false)
    private Integer hypertableId;

    @Column(name = "schema_name")
    private String schemaName;

    @Column(name = "table_name")
    private String tableName;

    @Column(name = "created")
    private Boolean created;

    public Integer getHypertableId() {
        return hypertableId;
    }

    public void setHypertableId(Integer hypertableId) {
        this.hypertableId = hypertableId;
    }

    public String getSchemaName() {
        return schemaName;
    }

    public void setSchemaName(String schemaName) {
        this.schemaName = schemaName;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public Boolean getCreated() {
        return created;
    }

    public void setCreated(Boolean created) {
        this.created = created;
    }
}
