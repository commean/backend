package eu.commean.backend.repo;

import eu.commean.backend.entity.TimeScaleDB;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface TimeScaleDBRepository extends PagingAndSortingRepository<TimeScaleDB, Integer> {

    TimeScaleDB createHypertable();

    @Transactional
    @Modifying
    void createContinuousAggregate();

    boolean checkForContinuousAggregate();

    boolean checkForHypertable();

    @Transactional
    @Modifying
    void setRealTimeUpdate(@Param(value = "state") boolean enable);

    int setRefreshPolicy();

}