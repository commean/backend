package eu.commean.backend.repo;

import eu.commean.backend.entity.Node;
import eu.commean.backend.entity.TrafficMeasurement;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface TrafficMeasurementRepository extends CrudRepository<TrafficMeasurement, UUID> {
	Iterable<TrafficMeasurement> findAllByNode(Node tcn);

	@Transactional
	Iterable<TrafficMeasurement> findAllByTimespan(@Param(value = "id") UUID uuid, @Param(value = "days") int days, @Param(value = "hours") int hours, @Param(value = "minutes") int minutes, @Param(value = "seconds") int seconds);

	Optional<TrafficMeasurement> findLatestById(@Param(value = "nodeId") UUID nodeId,@Param(value = "lineId")UUID lineId);




}
