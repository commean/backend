package eu.commean.backend.repo;

import eu.commean.backend.entity.AverageTraffic;
import eu.commean.backend.enums.VehicleTypes;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.security.core.parameters.P;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface AverageTrafficRepository extends PagingAndSortingRepository<AverageTraffic, UUID> {

    Optional<AverageTraffic> getAverageMeasurmentOfQarterHour(@Param(value = "vehicle_type") VehicleTypes vT, @Param(value = "node_id") UUID uuid,@Param(value = "line_uuid") UUID lineId, @Param(value = "dow") int dow, @Param(value = "time") String time);

    Optional<AverageTraffic> getAverageMeasurmentOfQarterHourAllTypes(@Param(value = "node_id") UUID uuid, @Param(value = "line_id") UUID lineId, @Param(value = "dow") int dow, @Param(value = "time") String time);
}
