package eu.commean.backend.repo;

import eu.commean.backend.entity.Line;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface LineRepository extends CrudRepository<Line, UUID> {
    boolean existsByLineId(UUID lineId);

    Optional<Line> findByLineId(UUID lineId);


}
