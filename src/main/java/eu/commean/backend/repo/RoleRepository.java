package eu.commean.backend.repo;

import eu.commean.backend.entity.Role;
import eu.commean.backend.enums.RoleName;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface RoleRepository extends CrudRepository<Role, UUID> {
	Role findByName(RoleName name);

}