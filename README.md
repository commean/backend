![beta](https://img.shields.io/docker/v/commean/backend/beta?label=beta&style=for-the-badge) ![alpha](https://img.shields.io/docker/v/commean/backend/alpha?label=alpha&style=for-the-badge)

# Backend
Backend for the Commean project.

## Compile
Use the following script to generate the `.jar`.
```sh
scripts/3_package.sh
```

## Start
The database has to be started BEFORE the Backend.
```sh
scripts/1_startDB.sh
scripts/4_run.sh
```

## Docker and docker-compose
For easy deployment commeans backend is avalible as a docker image. In order to run the container, modifiy the secret-template.env and rename it to secret.env. Then execute one of the following:

### Build from Source
```sh
docker-compose build
docker-compose up -d
```
### Latest from docker.io
```sh
docker-compose up -d
```
