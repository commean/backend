FROM docker.io/openjdk:17-alpine
ARG VERSION

RUN addgroup --system spring && adduser -S -s /bin/false -G spring spring

ADD target/backend-${VERSION}.jar /commean/backend.jar
RUN chown -R spring:spring /commean

WORKDIR /commean
USER spring
ENTRYPOINT ["java","-jar","/commean/backend.jar"]
