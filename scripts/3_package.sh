#!/bin/bash
. $(dirname "$0")/common.sh

check_for_app_yml

rm target/*.jar -rf
mvn $MAVEN_CLI_OPTS -DskipTests package
